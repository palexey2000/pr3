#include <vector>
#ifndef BUBBLE_SORT_H
#define BUBBLE_SORT_H

void bubbleSort(std::vector<int> &arr);

#endif // BUBBLE_SORT_H
