# Используем базовый образ с Docker
FROM docker:stable

# Устанавливаем необходимые пакеты
RUN apk --no-cache add \
    dpkg \
    && rm -rf /var/cache/apk/*

# Копируем .deb пакет в образ
COPY build/bin/*.deb /tmp/

# Устанавливаем .deb пакет
RUN dpkg -i /tmp/*.deb || true

# Запускаем Docker внутри контейнера
CMD ["dockerd", "--host=unix:///var/run/docker.sock"]
